package com.epam.newsmanagement.utils;

public class RequestMappingNames {
    public static final String RESET_FILTER = "reset-filter";
    public static final String NEWS_FILTER = "news-filter";
    public static final String NEWS_PAGE_NUMBER_PARAM = "/{pageNumber}";
    public static final String NEWS_FILTER_VIEW = "main";
    public static final String SINGLE_NEWS="single-news/{newsId}";
    public static final String NEWS_SINGLE_VIEW = "single-news-view";
    public static final String ADD_COMMENT = "add-comment";

    public static final String ERROR = "error";
}
