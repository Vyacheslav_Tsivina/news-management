package com.epam.newsmanagement.controller;

import com.epam.news.entity.*;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.IAuthorService;
import com.epam.news.service.INewsManageService;
import com.epam.news.service.INewsService;
import com.epam.news.service.ITagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

import static com.epam.newsmanagement.utils.RequestMappingNames.*;
/**
 * Class provides actions:
 * - get news by filter and page
 * - reset filter
 * - show single news by id
 */
@Controller
public class NewsController {

    @Autowired
    ITagService tagService;
    @Autowired
    IAuthorService authorService;
    @Autowired
    INewsService newsService;
    @Autowired
    INewsManageService newsManageService;

    private static final int NEWS_ON_PAGE = 3;

    @RequestMapping(value=NEWS_FILTER+ NEWS_PAGE_NUMBER_PARAM)
    public ModelAndView newsFilter(HttpSession session,
                                   @RequestParam(value = "author", required = false) Long authorId,
                                   @RequestParam(value = "tags", required = false) List<Long> tagIds,
                                   @PathVariable Integer pageNumber,
                                   @RequestParam(value = "fromFilter", required = false) Boolean fromFilter) throws ServiceException {
        return newsFilterModel(session, authorId, tagIds, pageNumber,fromFilter);
    }

    @RequestMapping(value=NEWS_FILTER)
    public ModelAndView newsFilter(HttpSession session,
                                   @RequestParam(value = "author", required = false) Long authorId,
                                   @RequestParam(value = "tags", required = false) List<Long> tagIds,
                                   @RequestParam(value = "fromFilter", required = false) Boolean fromFilter
    ) throws ServiceException {
        return newsFilterModel(session, authorId, tagIds, 1, fromFilter);
    }

    @RequestMapping(value=RESET_FILTER)
    public ModelAndView resetFilter(HttpSession session) throws ServiceException {
        session.setAttribute("authorId",null);
        session.setAttribute("tagIds",null);
        return newsFilterModel(session, null, null, 1, null);
    }
    @RequestMapping(value = SINGLE_NEWS)
    public ModelAndView singleNews(HttpSession session,
                                   @PathVariable Long newsId) throws ServiceException
    {
        FilterVO filterVO = getFilterFromSession(session, null, null);
        NewsVO newsVO = newsManageService.findNewsVO(newsId);
        int newsNumber = newsService.newsNumberFromFilter(newsId,filterVO);
        Long prevId = null;
        Long nextId = null;
        if (newsNumber > 1){
            prevId = newsService.newsFromFilterByNumber(newsNumber-1,filterVO).getId();
        }
        if (newsNumber < newsService.countNewsByFilter(filterVO)){
            nextId = newsService.newsFromFilterByNumber(newsNumber+1,filterVO).getId();
        }

        session.setAttribute("newsVO",newsVO);
        session.setAttribute("prevId",prevId);
        session.setAttribute("nextId",nextId);
        session.setAttribute("pageNumber",(newsNumber-1) / NEWS_ON_PAGE + 1);
        return new ModelAndView(NEWS_SINGLE_VIEW);
    }
    /**
     * Set attributes for view
     */
    private void prepareView(HttpSession session) throws ServiceException {
        List<Author> authorList = authorService.findAllAuthors();
        List<Tag> tagsList = tagService.findAllTags();
        session.setAttribute("tagsList",tagsList);
        session.setAttribute("authorList",authorList);
    }
    /**
     * General method for newsFilter
     */
    private ModelAndView newsFilterModel(HttpSession session,
                                         Long authorId,
                                         List<Long> tagIds,
                                         Integer pageNumber,
                                         Boolean fromFilter) throws ServiceException {
        if (fromFilter != null){//if request came not from filter button
            session.setAttribute("authorId",authorId);
            session.setAttribute("tagIds", tagIds);
        }
        if (fromFilter != null && tagIds == null) {//if we have 0 tags from filter
            session.setAttribute("tagIds",null);
        }

        prepareView(session);
        FilterVO filterVO = getFilterFromSession(session,authorId,tagIds);

        List<News> newsList;
        int newsCount = newsService.countNewsByFilter(filterVO);
        if (pageNumber == null) {//if use filter
            newsList = newsService.findNewsByFilters(NEWS_ON_PAGE, pageNumber, filterVO);
        } else {//if change page
            newsList = newsService.findNewsByFilters(NEWS_ON_PAGE, pageNumber, filterVO);
        }

        List<NewsVO> newsVOList = newsManageService.buildNewsVOList(newsList);

        session.setAttribute("pageCount",newsCount / NEWS_ON_PAGE + (newsCount%NEWS_ON_PAGE==0?0:1));
        if (pageNumber == null){
            session.setAttribute("pageNumber",1);
        }else{
            session.setAttribute("pageNumber",pageNumber);
        }
        session.setAttribute("newsOnPage",NEWS_ON_PAGE);
        session.setAttribute("newsVOList",newsVOList);
        return new ModelAndView(NEWS_FILTER_VIEW);
    }
    /**
     * Exception handler
     */
    @ExceptionHandler(ServiceException.class)
    public ModelAndView serviceExceptionHandler()
    {
        return new ModelAndView(ERROR);
    }

    /**
     * Build filterVO by author and tags ids
     */
    private FilterVO getFilterFromSession(HttpSession session,
                                          Long authorId,
                                          List<Long> tagIds) throws ServiceException
    {

        if (tagIds == null) tagIds = (List<Long>) session.getAttribute("tagIds");
        if (authorId == null) authorId = (Long) session.getAttribute("authorId");
        List<Tag> tags = null;
        if (tagIds != null) {
            tags = new ArrayList<>();
            for (Long id : tagIds) {
                tags.add(tagService.findTag(id));
            }
        }
        Author author = authorId == null ? null : authorService.findAuthor(authorId);
        return new FilterVO(tags,author);
    }
}
