package com.epam.news.util;

public class ColumnNames {
    private ColumnNames(){};
    public static final String AUTHOR_ID="author_id";
    public static final String AUTHOR_NAME="name";
    public static final String EXPIRED ="expired";

    public static final String COMMENT_ID="comment_id";
    public static final String COMMENT_TEXT="comment_text";
    public static final String CREATION_DATE="creation_date";
    public static final String COMMENT_NEWS_ID = "news_id";

    public static final String NEWS_ID="newsId";
    public static final String NEWS_SHORT_TEXT="newsShortText";
    public static final String NEWS_FULL_TEXT="newsFullText";
    public static final String NEWS_TITLE="newsTitle";
    public static final String NEWS_CREATION_DATE="newsCreationDate";
    public static final String NEWS_MODIFICATION_DATE="newsModificationDate";

    public static final String TAG_ID="tag_id";
    public static final String TAG_NAME="tag_name";

    public static final String USER_ID="user_id";
    public static final String USER_NAME="user_name";
    public static final String LOGIN="login";
    public static final String PASSWORD="password";
}
