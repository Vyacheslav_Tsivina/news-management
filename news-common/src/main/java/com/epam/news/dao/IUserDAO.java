package com.epam.news.dao;

import com.epam.news.entity.User;
import com.epam.news.exception.DAOException;

import java.util.List;
/**
 * Interface that provides C.R.U.D. operations with User
 *
 */
public interface IUserDAO {
    /**
     * Finds all users
     * @return list of users
     * @throws DAOException
     */
    List<User> findAll() throws DAOException;

    /**
     * Finds user by id
     * @param id
     * @return user
     * @throws DAOException
     */
    User findById(Long id) throws DAOException;

    /**
     * Deletes user by id
     * @param id
     * @return true if delete is successful, returns false otherwise
     * @throws DAOException
     */
    boolean delete(Long id) throws DAOException;

    /**
     * Creates user with given info
     * @param entity
     * @return inserted id
     * @throws DAOException
     */
    Long add(User entity) throws DAOException;

    /**
     * Updates user info by id
     * @param entity
     * @return true if update is successful, returns false otherwise
     * @throws DAOException
     */
    boolean update(User entity) throws DAOException;

    /**
     * Finds user by login
     * @param login
     * @return
     * @throws DAOException
     */
    User findByLogin(String login) throws DAOException;

    /**
     * Finds user role
     * @param id
     * @return
     * @throws DAOException
     */
    String findUserRole(Long id) throws DAOException;
}
