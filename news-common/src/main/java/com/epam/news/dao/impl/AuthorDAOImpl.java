package com.epam.news.dao.impl;

import com.epam.news.dao.IAuthorDAO;
import com.epam.news.entity.Author;
import com.epam.news.exception.DAOException;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.news.util.ColumnNames.*;
/**
 * Class that realize C.R.U.D. operations with Author using JDBC
 *
 */
public class AuthorDAOImpl implements IAuthorDAO{

    private DataSource dataSource;

    private static final String FIND_ALL="SELECT author_id,name,expired FROM Author ORDER BY name";
    private static final String FIND_BY_ID="SELECT author_id,name,expired FROM Author WHERE author_id=?";
    private static final String DELETE_BY_ID="DELETE FROM Author WHERE author_id=?";
    private static final String INSERT_BY_ENTITY="INSERT INTO Author(author_id,name,expired) VALUES (AUTHOR_SEQ.nextVal,?,?)";
    private static final String UPDATE_BY_ID="UPDATE Author SET name=?,expired=? WHERE author_id=?";
    private static final String FIND_AUTHOR_ID_BY_NEWS_ID = "SELECT author_id FROM NEWS_AUTHOR where news_id=?";
    /**
     * Finds all authors
     * @return List of authors
     * @throws DAOException
     */
    @Override
    public List<Author> findAll() throws DAOException {
        List<Author> resultList = new ArrayList<Author>();
        ResultSet rs = null;
        Statement st = null;
        Connection connection = null;
        try
        {
            connection = DataSourceUtils.getConnection(dataSource);
            st = connection.createStatement();
            rs=st.executeQuery(FIND_ALL);

            while(rs.next())
            {
                Author author= buildAuthor(rs);
                resultList.add(author);
            }
        }catch(SQLException e)
        {
            throw new DAOException(e);
        }finally
        {
            closeStatement(st);
            closeResultSet(rs);
            closeConnection(connection);
        }
        return resultList;
    }
    /**
     * Finds author by id
     * @param id
     * @return author if find is successful, returns null otherwise
     * @throws DAOException
     */
    @Override
    public Author findById(Long id) throws DAOException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Author result=null;
        Connection connection = null;
        try
        {
            connection = DataSourceUtils.getConnection(dataSource);
            ps=connection.prepareStatement(FIND_BY_ID);
            ps.setLong(1, id);
            rs=ps.executeQuery();

            if(rs.next())
            {
                result= buildAuthor(rs);
            }
        }catch(SQLException e)
        {
            throw new DAOException(e);
        }finally
        {
            closeResultSet(rs);
            closePreparedStatement(ps);
            closeConnection(connection);
        }
        return result;
    }
    /**
     * Deletes author by id
     * @param id
     * @return true if delete is successful, returns false otherwise
     * @throws DAOException
     */
    @Override
    public boolean delete(Long id) throws DAOException {
        PreparedStatement ps = null;
        Connection connection = null;
        int deleteFlag = 0;
        try
        {
            connection = DataSourceUtils.getConnection(dataSource);
            ps=connection.prepareStatement(DELETE_BY_ID);
            ps.setLong(1, id);
            deleteFlag = ps.executeUpdate();
        }catch(SQLException e)
        {
            throw new DAOException(e);
        }finally
        {
            closePreparedStatement(ps);
            closeConnection(connection);
        }
        return deleteFlag == 1;
    }
    /**
     * Creates author with given info
     * @param entity
     * @return id of inserted author, 0 if insert was unsuccessful
     * @throws DAOException
     */
    @Override
    public Long add(Author entity) throws DAOException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection connection = null;
        Long insertId = 0L;
        try
        {
            connection = DataSourceUtils.getConnection(dataSource);
            String[] id = {"author_id"};
            ps=connection.prepareStatement(INSERT_BY_ENTITY,id);
            ps.setString(1, entity.getName());
            Timestamp expired = dateToTimeStamp(entity.getExpired());
            ps.setTimestamp(2, expired);
            ps.execute();
            rs = ps.getGeneratedKeys();
            if (rs.next())
            {
                insertId = rs.getLong(1);
            }
        }catch(SQLException e)
        {
            throw new DAOException(e);
        }finally
        {
            closeResultSet(rs);
            closePreparedStatement(ps);
            closeConnection(connection);
        }
        return insertId;
    }
    /**
     * Updates author info by id
     * @param entity
     * @return true if update is successful, returns false otherwise
     * @throws DAOException
     */
    @Override
    public boolean update(Author entity) throws DAOException {
        int updateFlag=0;
        PreparedStatement ps = null;
        Connection connection = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(UPDATE_BY_ID);

            ps.setString(1, entity.getName());
            Timestamp expired = dateToTimeStamp(entity.getExpired());
            ps.setTimestamp(2, expired);
            ps.setLong(3, entity.getId());
            updateFlag =ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        }finally {
            closePreparedStatement(ps);
            closeConnection(connection);
        }
        return updateFlag == 1;
    }

    /**
     * Finds news author
     * @param id of news
     * @return
     * @throws DAOException
     */
    @Override
    public Author findNewsAuthor(Long id) throws DAOException {
        Author result = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        Connection connection = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(FIND_AUTHOR_ID_BY_NEWS_ID);
            ps.setLong(1, id);
            rs = ps.executeQuery();
            if (rs.next())
            {
                result = findById(rs.getLong(AUTHOR_ID));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }finally {
            closeResultSet(rs);
            closePreparedStatement(ps);
            closeConnection(connection);
        }
        return result;
    }

    /**
     * Closes prepared statement
     * @param ps
     * @throws DAOException
     */
    private void closePreparedStatement(PreparedStatement ps) throws DAOException
    {
        try
        {
            if (ps != null)
            {
                ps.close();
            }
        }catch(SQLException e){
            throw new DAOException(e);
        }
    }
    /**
     * Closes statement
     * @param st
     * @throws DAOException
     */
    private void closeStatement(Statement st) throws DAOException
    {
        try
        {
            if (st != null)
            {
                st.close();
            }
        }catch(SQLException e){
            throw new DAOException(e);
        }
    }
    /**
     * Closes result set
     * @param rs
     * @throws DAOException
     */
    private void closeResultSet(ResultSet rs) throws DAOException
    {
        try
        {
            if (rs != null)
            {
                rs.close();
            }
        }catch(SQLException e){
            throw new DAOException(e);
        }
    }
    /**
     * Closes connection
     * @param cn
     * @throws DAOException
     */
    private void closeConnection(Connection cn)
    {
        if (cn != null)
        {
            DataSourceUtils.releaseConnection(cn, dataSource);
        }
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    /**
     * Builds author from ResultSet
     * @throws SQLException
     */
    private Author buildAuthor(ResultSet rs) throws SQLException
    {
        return new Author(rs.getLong(AUTHOR_ID),rs.getString(AUTHOR_NAME),rs.getTimestamp(EXPIRED));
    }

    /**
     * Converts date to Timestamp
     * @param date
     * @return
     */
    private Timestamp dateToTimeStamp(java.util.Date date)
    {
        return date == null? null: new Timestamp(date.getTime());
    }
}
