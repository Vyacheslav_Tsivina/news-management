package com.epam.news.dao;

import com.epam.news.entity.Comment;
import com.epam.news.exception.DAOException;

import java.util.List;
/**
 * Interface that provides C.R.U.D. operations with Comment 
 *
 */
public interface ICommentDAO {
    /**
     * Finds all comments
     * @return List of comments
     * @throws DAOException
     */
    List<Comment> findAll() throws DAOException;
    /**
     * Finds comment by id
     * @param id
     * @return comment if find is successful, returns null otherwise
     * @throws DAOException
     */
    Comment findById(Long id) throws DAOException;
    /**
     * Deletes comment by id
     * @param id of comment
     * @return true if delete is successful, returns false otherwise
     * @throws DAOException
     */
    boolean delete(Long id) throws DAOException;
    /**
     * Creates comment with given info
     * @param entity
     * @return id of inserted comment
     * @throws DAOException
     */
    Long add(Comment entity) throws DAOException;
    /**
     * Updates comment info by id
     * @param entity
     * @return true if update is successful, returns false otherwise
     * @throws DAOException
     */
    boolean update(Comment entity) throws DAOException;
    /**
     * Deletes all comments for news with given id
     * @param id of news
     * @return true if delete is successful, returns false otherwise
     * @throws DAOException
     */
    boolean deleteCommentsForNews(Long id) throws DAOException;
    /**
     * Finds all comments for news with given id
     * @param id of news
     * @return List of comments
     * @throws DAOException
     */
    List<Comment> findCommentsForNews(Long id) throws DAOException;
}
