package com.epam.news.dao;

import java.util.List;

import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import org.springframework.transaction.annotation.Transactional;

/**
 * Interface that provides C.R.U.D. operations with Tag
 *
 */
@Transactional
public interface ITagDAO {
    /**
     * Finds all tags
     * @return List of tags
     * @throws DAOException
     */
    List<Tag> findAll() throws DAOException;
    /**
     * Finds tag by id
     * @param id
     * @return tag if find is successful, returns null otherwise
     * @throws DAOException
     */
    Tag findById(Long id) throws DAOException;
    /**
     * Deletes tag by id
     * @param id of tag
     * @return true if delete is successful, returns false otherwise
     * @throws DAOException
     */
    boolean delete(Long id) throws DAOException;
    /**
     * Creates tag with given info
     * @param entity
     * @return id of inserted tag
     * @throws DAOException
     */
    Long add(Tag entity) throws DAOException;
    /**
     * Updates tag info by id
     * @param entity
     * @return true if update is successful, returns false otherwise
     * @throws DAOException
     */
    boolean update(Tag entity) throws DAOException;
    /**
     * Finds all tags for news with given id
     * @param id of news
     * @return list of tags
     * @throws DAOException
     */
    List<Tag> findTagsForNews(Long id) throws DAOException;

    /**
     * Deletes tag with given id from all news
     * @param id od tag
     * @throws DAOException
     */
    void deleteTagFromNews(Long id) throws DAOException;
}
