package com.epam.news.dao;

import java.util.List;

import com.epam.news.entity.Author;
import com.epam.news.exception.DAOException;
/**
 * Interface that realize C.R.U.D. operations with Author 
 *
 */
public interface IAuthorDAO {
    /**
     * Finds all authors
     * @return List of authors
     * @throws DAOException
     */
    List<Author> findAll() throws DAOException;
    /**
     * Finds author by id
     * @param id
     * @return author if find is successful, returns null otherwise
     * @throws DAOException
     */
    Author findById(Long id) throws DAOException;
    /**
     * Deletes author by id
     * @param id
     * @return true if delete is successful, returns false otherwise
     * @throws DAOException
     */
    boolean delete(Long id) throws DAOException;
    /**
     * Creates author with given info
     * @param entity
     * @return id of inserted author
     * @throws DAOException
     */
    Long add(Author entity) throws DAOException;
    /**
     * Updates author info by id
     * @param entity
     * @return true if update is successful, returns false otherwise
     * @throws DAOException
     */
    boolean update(Author entity) throws DAOException;

    /**
     * Finds news author
     * @param id of news
     * @return
     * @throws DAOException
     */
    Author findNewsAuthor(Long id) throws DAOException;

}
