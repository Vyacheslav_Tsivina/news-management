package com.epam.news.dao.impl;

import com.epam.news.dao.IUserDAO;
import com.epam.news.entity.User;
import com.epam.news.exception.DAOException;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.news.util.ColumnNames.*;

/**
 * Class that provides C.R.U.D. operations with User using JDBC
 */
public class UserDAOImpl implements IUserDAO {

    private DataSource dataSource;

    private static final String FIND_ALL = "SELECT user_id,user_name,login,password,expired FROM USERS";
    private static final String FIND_BY_LOGIN = "SELECT user_id,user_name,login,password,expired FROM USERS WHERE login = ?";
    private static final String FIND_BY_ID = "SELECT user_id,user_name,login,password,expired FROM USERS WHERE user_id = ?";
    private static final String DELETE_BY_ID = "DELETE FROM USERS WHERE user_id = ?";
    private static final String INSERT_BY_ENTITY = "INSERT INTO USERS(user_id,user_name,login,password,expired) " +
            "VALUES (TAG_SEQ.nextVal,?,?,?,?)";
    private static final String UPDATE_BY_ID = "UPDATE USERS SET user_name=?,login=?,password=?,expired=? WHERE user_id=?";
    private static final String FIND_USER_ROLE = "SELECT role_name FROM ROLES WHERE user_id=?";

    /**
     * Finds all users
     * @return list of users
     * @throws DAOException
     */
    @Override
    public List<User> findAll() throws DAOException {
        List<User> resultList = new ArrayList<>();
        Connection connection = null;
        Statement st=  null;
        ResultSet rs = null;
        try
        {
            connection = DataSourceUtils.getConnection(dataSource);
            st=connection.createStatement();
            rs=st.executeQuery(FIND_ALL);

            while(rs.next())
            {
                User user= buildUser(rs);
                resultList.add(user);
            }
        }catch(SQLException e)
        {
            throw new DAOException(e);
        }finally
        {
            closeResultSet(rs);
            closeStatement(st);
            closeConnection(connection);
        }
        return resultList;
    }
    /**
     * Finds user by id
     * @param id
     * @return user
     * @throws DAOException
     */
    @Override
    public User findById(Long id) throws DAOException {
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet rs = null;
        User result = null;
        try
        {
            connection = DataSourceUtils.getConnection(dataSource);
            ps=connection.prepareStatement(FIND_BY_ID);
            ps.setLong(1, id);
            rs=ps.executeQuery();

            if(rs.next())
            {
                result = buildUser(rs);
            }
        }catch(SQLException e)
        {
            throw new DAOException(e);
        }finally
        {
            closeResultSet(rs);
            closePreparedStatement(ps);
            closeConnection(connection);
        }
        return result;
    }
    /**
     * Deletes user by id
     * @param id
     * @return true if delete is successful, returns false otherwise
     * @throws DAOException
     */
    @Override
    public boolean delete(Long id) throws DAOException {
        PreparedStatement ps=null;
        Connection connection = null;
        int deleteFlag=0;
        try
        {
            connection = DataSourceUtils.getConnection(dataSource);
            ps=connection.prepareStatement(DELETE_BY_ID);
            ps.setLong(1, id);
            deleteFlag = ps.executeUpdate();
        }catch(SQLException e)
        {
            throw new DAOException(e);
        }finally
        {
            closePreparedStatement(ps);
            closeConnection(connection);
        }
        return deleteFlag == 1;
    }
    /**
     * Creates user with given info
     * @param entity
     * @return inserted id
     * @throws DAOException
     */
    @Override
    public Long add(User entity) throws DAOException {
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet rs = null;
        Long insertId = 0L;
        try
        {
            connection = DataSourceUtils.getConnection(dataSource);
            String[] id = {"user_id"};
            ps=connection.prepareStatement(INSERT_BY_ENTITY,id);
            ps.setString(1, entity.getName());
            ps.setString(2,entity.getLogin());
            ps.setString(3,entity.getPassword());
            Timestamp expired = entity.getExpired() == null? null : new Timestamp(entity.getExpired().getTime());
            ps.setTimestamp(4,expired);
            ps.execute();
            rs = ps.getGeneratedKeys();
            if (rs.next())
            {
                insertId = rs.getLong(1);
            }
        }catch(SQLException e)
        {
            throw new DAOException(e);
        }finally
        {
            closeResultSet(rs);
            closePreparedStatement(ps);
            closeConnection(connection);
        }
        return insertId;
    }
    /**
     * Finds user by login
     * @param login
     * @return
     * @throws DAOException
     */
    @Override
    public User findByLogin(String login) throws DAOException {
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet rs = null;
        User result = null;
        try
        {
            connection = DataSourceUtils.getConnection(dataSource);
            ps=connection.prepareStatement(FIND_BY_LOGIN);
            ps.setString(1,login);
            rs=ps.executeQuery();

            if(rs.next())
            {
                result = buildUser(rs);
            }
        }catch(SQLException e)
        {
            throw new DAOException(e);
        }finally
        {
            closeResultSet(rs);
            closePreparedStatement(ps);
            closeConnection(connection);
        }
        return result;
    }
    /**
     * Updates user info by id
     * @param entity
     * @return true if update is successful, returns false otherwise
     * @throws DAOException
     */
    @Override
    public boolean update(User entity) throws DAOException {
        int updateFlag = 0;
        PreparedStatement ps = null;
        Connection connection = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(UPDATE_BY_ID);
            ps.setString(1, entity.getName());
            ps.setString(2, entity.getLogin());
            ps.setString(3, entity.getPassword());
            Timestamp expired = dateToTimeStamp(entity.getExpired());
            ps.setTimestamp(4, expired);
            ps.setLong(5, entity.getId());
            updateFlag =ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        }finally {
            closePreparedStatement(ps);
            closeConnection(connection);
        }
        return updateFlag == 1;
    }
    /**
     * Finds user role
     * @param id
     * @return
     * @throws DAOException
     */
    @Override
    public String findUserRole(Long id) throws DAOException {
        String role = null;
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet rs = null;
        try{
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(FIND_USER_ROLE);
            ps.setLong(1, id);
            rs=ps.executeQuery();

            if(rs.next())
            {
                role = rs.getString(1);
            }
        }catch(SQLException e)
        {
            throw new DAOException(e);
        }finally
        {
            closeResultSet(rs);
            closePreparedStatement(ps);
            closeConnection(connection);
        }
        return role;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private User buildUser(ResultSet rs) throws SQLException {
        return new User(rs.getLong(USER_ID),
                rs.getString(USER_NAME),
                rs.getString(LOGIN),
                rs.getString(PASSWORD),
                rs.getTimestamp(EXPIRED));
    }



    /**
     * Closes prepared statement
     * @param ps
     * @throws DAOException
     */
    private void closePreparedStatement(PreparedStatement ps) throws DAOException
    {
        try
        {
            if (ps != null)
            {
                ps.close();
            }
        }catch(SQLException e){
            throw new DAOException(e);
        }
    }
    /**
     * Closes statement
     * @param st
     * @throws DAOException
     */
    private void closeStatement(Statement st) throws DAOException
    {
        try
        {
            if (st != null)
            {
                st.close();
            }
        }catch(SQLException e){
            throw new DAOException(e);
        }
    }
    /**
     * Closes result set
     * @param rs
     * @throws DAOException
     */
    private void closeResultSet(ResultSet rs) throws DAOException
    {
        try
        {
            if (rs != null)
            {
                rs.close();
            }
        }catch(SQLException e){
            throw new DAOException(e);
        }
    }
    /**
     * Closes connection
     * @param cn
     * @throws DAOException
     */
    private void closeConnection(Connection cn)
    {
        if (cn != null)
        {
            DataSourceUtils.releaseConnection(cn, dataSource);
        }
    }

    /**
     * Converts date to Timestamp
     * @param date
     * @return
     */
    private Timestamp dateToTimeStamp(java.util.Date date)
    {
        return date == null? null: new Timestamp(date.getTime());
    }
}
