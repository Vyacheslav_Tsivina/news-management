package com.epam.news.dao.impl;

import com.epam.news.dao.ITagDAO;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.news.util.ColumnNames.TAG_ID;
import static com.epam.news.util.ColumnNames.TAG_NAME;
/**
 * Class that provides C.R.U.D. operations with Tag using JDBC
 *
 */
@Transactional
public class TagDAOImpl implements ITagDAO{

    private DataSource dataSource;

    private static final String FIND_ALL="SELECT tag_id,tag_name FROM Tag ORDER BY tag_name";
    private static final String FIND_BY_ID="SELECT tag_id,tag_name FROM Tag WHERE tag_id=?";
    private static final String DELETE_BY_ID="DELETE FROM Tag WHERE tag_id=?";
    private static final String INSERT_BY_ENTITY="INSERT INTO Tag(tag_id,tag_name) VALUES (TAG_SEQ.nextVal,?)";
    private static final String UPDATE_BY_ID="UPDATE Tag SET tag_name=? WHERE tag_id=?";
    private static final String FING_TAGS_BY_NEWS_ID="SELECT TAG.tag_id, TAG.tag_name "+
            "FROM TAG "+
            "JOIN NEWS_TAG ON TAG.TAG_ID = NEWS_TAG.TAG_ID "+
            "WHERE NEWS_TAG.NEWS_ID = ?";
    private static final String DELETE_TAG_FROM_NEWS = "DELETE FROM NEWS_TAG WHERE tag_id = ?";
    /**
     * Finds all tags
     * @return List of tags
     * @throws DAOException
     */
    @Override
    public List<Tag> findAll() throws DAOException {
        List<Tag> resultList = new ArrayList<Tag>();
        Connection connection = null;
        Statement st=  null;
        ResultSet rs = null;
        try
        {
            connection = DataSourceUtils.getConnection(dataSource);
            st=connection.createStatement();
            rs=st.executeQuery(FIND_ALL);

            while(rs.next())
            {
                Tag tag= buildTag(rs);
                resultList.add(tag);
            }
        }catch(SQLException e)
        {
            throw new DAOException(e);
        }finally
        {
            closeResultSet(rs);
            closeStatement(st);
            closeConnection(connection);
        }
        return resultList;
    }
    /**
     * Finds tag by id
     * @param id
     * @return tag if find is successful, returns null otherwise
     * @throws DAOException
     */
    @Override
    public Tag findById(Long id) throws DAOException {
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet rs = null;
        Tag result = null;
        try
        {
            connection = DataSourceUtils.getConnection(dataSource);
            ps=connection.prepareStatement(FIND_BY_ID);
            ps.setLong(1, id);
            rs=ps.executeQuery();

            if(rs.next())
            {
                result = buildTag(rs);
            }
        }catch(SQLException e)
        {
            throw new DAOException(e);
        }finally
        {
            closeResultSet(rs);
            closePreparedStatement(ps);
            closeConnection(connection);
        }
        return result;
    }
    /**
     * Deletes tag by id
     * @param id of tag
     * @return true if delete is successful, returns false otherwise
     * @throws DAOException
     */
    @Override
    public boolean delete(Long id) throws DAOException {
        PreparedStatement ps=null;
        Connection connection = null;
        int deleteFlag=0;
        try
        {
            connection = DataSourceUtils.getConnection(dataSource);
            ps=connection.prepareStatement(DELETE_BY_ID);
            ps.setLong(1, id);
            deleteFlag = ps.executeUpdate();
        }catch(SQLException e)
        {
            throw new DAOException(e);
        }finally
        {
            closePreparedStatement(ps);
            closeConnection(connection);
        }
        return deleteFlag == 1;
    }
    /**
     * Creates tag with given info
     * @param entity
     * @return id of inserted tag, 0 if insert was unsuccessful
     * @throws DAOException
     */
    @Override
    public Long add(Tag entity) throws DAOException {
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet rs = null;
        Long insertId = 0L;
        try
        {
            connection = DataSourceUtils.getConnection(dataSource);
            String[] id = {"tag_id"};
            ps=connection.prepareStatement(INSERT_BY_ENTITY,id);
            ps.setString(1, entity.getName());
            ps.execute();
            rs = ps.getGeneratedKeys();
            if (rs.next())
            {
                insertId = rs.getLong(1);
            }
        }catch(SQLException e)
        {
            throw new DAOException(e);
        }finally
        {
            closeResultSet(rs);
            closePreparedStatement(ps);
            closeConnection(connection);
        }
        return insertId;
    }
    /**
     * Updates tag info by id
     * @param entity
     * @return true if update is successful, returns false otherwise
     * @throws DAOException
     */
    @Override
    public boolean update(Tag entity) throws DAOException {
        int updateFlag = 0;
        PreparedStatement ps = null;
        Connection connection = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(UPDATE_BY_ID);
            ps.setString(1, entity.getName());
            ps.setLong(2, entity.getId());
            updateFlag =ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        }finally {
            closePreparedStatement(ps);
            closeConnection(connection);
        }
        return updateFlag == 1;
    }
    /**
     * Finds all tags for news with given id
     * @param id of news
     * @return list of tags
     * @throws DAOException
     */
    @Override
    public List<Tag> findTagsForNews(Long id) throws DAOException {
        List<Tag> resultList = new ArrayList<Tag>();
        PreparedStatement ps = null;
        Connection connection = null;
        ResultSet rs = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            ps = connection.prepareStatement(FING_TAGS_BY_NEWS_ID);
            ps.setLong(1, id);

            rs = ps.executeQuery();
            while(rs.next())
            {
                Tag tag= buildTag(rs);
                resultList.add(tag);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }finally {
            closePreparedStatement(ps);
            closeResultSet(rs);
            closeConnection(connection);
        }
        return resultList;
    }
    /**
     * Deletes tag with given id from all news
     * @param id of tag
     * @throws DAOException
     */
    @Override
    public void deleteTagFromNews(Long id) throws DAOException {
        PreparedStatement ps=null;
        Connection connection = null;
        try
        {
            connection = DataSourceUtils.getConnection(dataSource);
            ps=connection.prepareStatement(DELETE_TAG_FROM_NEWS);
            ps.setLong(1, id);
            ps.executeUpdate();
        }catch(SQLException e)
        {
            throw new DAOException(e);
        }finally
        {
            closePreparedStatement(ps);
            closeConnection(connection);
        }
    }

    /**
     * Closes prepared statement
     * @param ps
     * @throws DAOException
     */
    private void closePreparedStatement(PreparedStatement ps) throws DAOException
    {
        try
        {
            if (ps != null)
            {
                ps.close();
            }
        }catch(SQLException e){
            throw new DAOException(e);
        }
    }
    /**
     * Closes statement
     * @param st
     * @throws DAOException
     */
    private void closeStatement(Statement st) throws DAOException
    {
        try
        {
            if (st != null)
            {
                st.close();
            }
        }catch(SQLException e){
            throw new DAOException(e);
        }
    }
    /**
     * Closes result set
     * @param rs
     * @throws DAOException
     */
    private void closeResultSet(ResultSet rs) throws DAOException
    {
        try
        {
            if (rs != null)
            {
                rs.close();
            }
        }catch(SQLException e){
            throw new DAOException(e);
        }
    }
    /**
     * Closes connection
     * @param cn
     * @throws DAOException
     */
    private void closeConnection(Connection cn)
    {
        if (cn != null)
        {
            DataSourceUtils.releaseConnection(cn, dataSource);
        }
    }
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    /**
     * Builds tag from ResultSet
     * @throws SQLException
     */
    private Tag buildTag(ResultSet rs) throws SQLException
    {
        return new Tag(rs.getLong(TAG_ID),rs.getString(TAG_NAME));
    }

}
