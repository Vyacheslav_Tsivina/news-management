package com.epam.news.dao;

import java.util.List;

import com.epam.news.entity.Author;
import com.epam.news.entity.FilterVO;
import com.epam.news.entity.News;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;

/**
 * Interface that provides C.R.U.D. operations with News and related information
 *
 */
public interface INewsDAO {

    /**
     * Finds news by id
     * @param id
     * @return
     * @throws DAOException
     */
    News findById(Long id) throws DAOException;
    /**
     * Deletes news by id
     * @param id of news
     * @return true if delete is successful, returns false otherwise
     * @throws DAOException
     */
    boolean delete(Long id) throws DAOException;
    /**
     * Creates news with given info
     * @param entity
     * @return id of inserted comment
     * @throws DAOException
     */
    Long add(News entity) throws DAOException;
    /**
     * Updates news info by id
     * @param entity
     * @return true if update is successful, returns false otherwise
     * @throws DAOException
     */
    boolean update(News entity) throws DAOException;
    /**
     * Inserts info about news author
     * @param newsId
     * @param authorId
     * @return true if insert is successful, returns false otherwise
     * @throws DAOException
     */
    boolean insertNewsAuthor(Long newsId,Long authorId) throws DAOException;
    /**
     * Checks if there is already such tag(tags) for news and then insert it if it needed
     * @param newsId news id
     * @param tags ArrayList of tags to be added
     * @throws DAOException
     */
    void insertNewsTags(Long newsId, List<Tag> tags) throws DAOException;
    /**
     * Finds all news from given author
     * @param author
     * @return List of all news by author
     * @throws DAOException
     */
    List<News> findByAuthor(Author author) throws DAOException;

    /**
     * Deletes all tags for news
     * @param id of news
     * @return true if delete is successful, returns false otherwise
     * @throws DAOException
     */
    boolean deleteNewsTags(Long id) throws DAOException;
    /**
     * Deletes news author by news id
     * @param id of news
     * @return true if delete is successful, returns false otherwise
     * @throws DAOException
     */
    boolean deleteNewsAuthor(Long id) throws DAOException;

    /**
     * Finds count of news by given tags, author, tags, page
     * @param filterVO
     * @return list of news
     * @throws DAOException
     */
    int countNewsByFilter(FilterVO filterVO) throws DAOException;

    /**
     * Finds news number in list of filtered news
     * @param newsId
     * @return news number
     * @throws DAOException
     */
    Integer newsNumberFromFilter(Long newsId, FilterVO filterVO) throws DAOException;

    /**
     * Finds news from filter by news number
     * @param newsNumber
     * @param filterVO
     * @return news
     * @throws DAOException
     */
    News newsFromFilterByNumber(int newsNumber, FilterVO filterVO) throws DAOException;
    /**
     * Returns list of news from filter on given page number.
     * Page numbers begin with 1. News ordered by comments number and modification date
     * @param newsOnPage how many news will be on page
     * @param pageNumber determine number of page which should be return
     * @param filterVO
     * @return list of news on given page
     */
    public List<News> findNewsByFilters(int pageNumber, int newsOnPage, FilterVO filterVO) throws DAOException;

}
