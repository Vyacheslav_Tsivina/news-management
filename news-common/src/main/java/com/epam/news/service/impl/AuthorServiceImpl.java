package com.epam.news.service.impl;

import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.dao.IAuthorDAO;
import com.epam.news.entity.Author;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.IAuthorService;
/**
 * Class that provides actions with authors
 *
 */
@Service
@Transactional(rollbackFor = {ServiceException.class, RuntimeException.class})
public class AuthorServiceImpl implements IAuthorService{

    private static Logger logger= Logger.getLogger(AuthorServiceImpl.class);
    private IAuthorDAO authorDAO ;

    /**
     * Adds new author and set inserted id into author
     * @param author to be added
     */
    @Override
    public void addAuthor(Author author) throws ServiceException {
        try{
            author.setId(authorDAO.add(author));
        }catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }

    }
    /**
     * Deletes given author by author id
     * @param id ot author
     */
    @Override
    public void deleteAuthor(Long id) throws ServiceException {
        try{
            authorDAO.delete(id);
        }catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }
    /**
     * Updates given author by author id
     * @param author to be updated
     * @throws ServiceException
     */
    @Override
    public void updateAuthor(Author author) throws ServiceException {
        try{
            authorDAO.update(author);
        }catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }

    }
    /**
     * Finds author by id
     * @param id
     * @return author or null
     * @throws ServiceException
     */
    @Override
    public Author findAuthor(Long id) throws ServiceException {
        Author author = null;
        try{
            author = authorDAO.findById(id);
        }catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return author;
    }
    /**
     * Finds all authors
     * @return list of authors
     * @throws ServiceException
     */
    @Override
    public List<Author> findAllAuthors() throws ServiceException {
        List<Author> result = null;
        try{
            result = authorDAO.findAll();
        }catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return result;
    }
    /**
     * Finds author of news
     * @param id of news
     * @return
     * @throws ServiceException
     */
    @Override
    public Author findNewsAuthor(Long id) throws ServiceException {
        Author result = null;
        try {
            result = authorDAO.findNewsAuthor(id);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return result;
    }

    public void setAuthorDAO(IAuthorDAO authorDAO) {
        this.authorDAO = authorDAO;
    }

}
