package com.epam.news.service;

import java.util.List;

import com.epam.news.dao.INewsDAO;
import com.epam.news.entity.Author;
import com.epam.news.entity.FilterVO;
import com.epam.news.entity.News;
import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;
/**
 * Interface that provides actions with news
 *
 */
public interface INewsService {
    /**
     * Adds news and set inserted id into news
     * @param news to be added
     */
    void addNews(News news) throws ServiceException;
    /**
     * Deletes given news by news id
     * @param id of news
     */
    void deleteNews(Long id) throws ServiceException;
    /**
     * Updates news information by news id
     * @param news to be edited
     */
    void updateNews(News news) throws ServiceException;

    /**
     * Finds all news written by given author
     * @param author by which the search is carried out
     * @return list of news
     */
    List<News> newsByAuthor(Author author) throws ServiceException;

    /**
     * Returns list of news from filter on given page number.
     * Page numbers begin with 1. News ordered by comments number and modification date
     * @param newsOnPage how many news will be on page
     * @param pageNumber determine number of page which should be return
     * @return list of news on given page
     */
    List<News> findNewsByFilters(int newsOnPage, int pageNumber, FilterVO filterVO) throws ServiceException;


    /**
     * Find news by id
     * @param id of news
     * @return news by given id
     */
    News findById(Long id) throws ServiceException;


    /**
     * Inserts info about news author
     * @param newsId
     * @param authorId
     * @return true if insert is successful, returns false otherwise
     * @throws ServiceException
     */
    boolean insertNewsAuthor(Long newsId,Long authorId) throws ServiceException;

    /**
     * Checks if there is already such tag(tags) for news and then insert it if it needed
     * @param newsId news id
     * @param tags ArrayList of tags to be added
     * @throws ServiceException
     */
    void insertNewsTags(Long newsId,List<Tag> tags) throws ServiceException;
    /**
     * Deletes news author by news id
     * @param id of news
     * @return true if delete is successful, returns false otherwise
     * @throws ServiceException
     */
    boolean deleteNewsAuthor(Long id) throws ServiceException;

    /**
     * Deletes all tags for news
     * @param id of news
     * @return true if delete is successful, returns false otherwise
     * @throws ServiceException
     */
    boolean deleteNewsTags(Long id) throws ServiceException;

    /**
     * Finds count of news by given tags, author, tags, pag
     * @return
     * @throws ServiceException
     */
    int countNewsByFilter(FilterVO filterVO) throws ServiceException;

    /**
     * Finds news from filter by news number
     * @param newsNumber
     * @return news
     * @throws ServiceException
     */
    News newsFromFilterByNumber(int newsNumber, FilterVO filterVO) throws ServiceException;

    /**
     * Finds news number in list of filtered news
     * @param newsId
     * @return news number
     * @throws ServiceException
     */
    Integer newsNumberFromFilter(Long newsId, FilterVO filterVO) throws ServiceException;

    void setNewsDAO(INewsDAO newsDAO);
}
