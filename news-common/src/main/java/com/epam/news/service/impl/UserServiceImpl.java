package com.epam.news.service.impl;

import com.epam.news.dao.IUserDAO;
import com.epam.news.entity.User;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.IUserService;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Class that provides actions with users
 */
@Service
@Transactional(rollbackFor = {ServiceException.class, RuntimeException.class})
public class UserServiceImpl implements IUserService{
    private static Logger logger= Logger.getLogger(UserServiceImpl.class);
    private IUserDAO userDAO;


    /**
     * Finds user by login
     * @param login
     * @return
     * @throws ServiceException
     */
    @Override
    public User findUser(String login) throws ServiceException{
        User result = null;
        try{
            result = userDAO.findByLogin(login);
        }catch (DAOException e){
            logger.error(e);
            throw new ServiceException(e);
        }
        return result;
    }
    /**
     * Finds user role by id
     * @param id
     * @return
     * @throws ServiceException
     */
    @Override
    public String findUserRole(Long id) throws ServiceException {
        String result = null;
        try{
            result = userDAO.findUserRole(id);
        }catch (DAOException e){
            logger.error(e);
            throw new ServiceException(e);
        }
        return result;
    }

    public void setUserDAO(IUserDAO userDAO) {
        this.userDAO = userDAO;
    }
}
