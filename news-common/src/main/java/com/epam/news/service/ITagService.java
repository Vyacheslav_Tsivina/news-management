package com.epam.news.service;

import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;

import java.util.List;

/**
 * Interface that provides actions with tags
 *
 */

public interface ITagService {
    /**
     * Adds new tag and set inserted id into tag
     * @param tag to be added
     */
    void addTag(Tag tag) throws ServiceException;
    /**
     * Deletes given tag by tag id
     * @param id of tag
     */
    void deleteTag(Long id) throws ServiceException;
    /**
     * Updates given tag by tag id
     * @param tag to be updated
     * @throws ServiceException
     */
    void updateTag(Tag tag) throws ServiceException;
    /**
     * Finds tag by id
     * @param id
     * @return tag or null
     * @throws ServiceException
     */
    Tag findTag(Long id) throws ServiceException;

    /**
     * Finds all tags for news with given id
     * @param id of news
     * @return list of tags
     * @throws ServiceException
     */
    List<Tag> findTagsForNews(Long id) throws ServiceException;

    /**
     * Finds all tags
     * @return list of tags
     * @throws ServiceException
     */
    List<Tag> findAllTags() throws ServiceException;
    /**
     * Deletes tag with given id from all news
     * @param id od tag
     * @throws ServiceException
     */
    void deleteTagFromNews(Long id) throws ServiceException;
}
