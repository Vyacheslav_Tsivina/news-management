package com.epam.news.service;

import com.epam.news.entity.News;
import com.epam.news.entity.NewsVO;
import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;

import java.util.List;

/**
 * Interface that provides complex actions with news
 *
 */
public interface INewsManageService {
    /**
     * Adds news with news info, author info, tags and comments
     * @param newsVO to add
     * @throws ServiceException
     */
    void add(NewsVO newsVO) throws ServiceException;

    /**
     * Deletes news according news id and all information about it(tags,comments)
     * @param id of newsVO
     * @throws ServiceException
     */
    void delete(Long id) throws ServiceException;
    /**
     * Collects all the data about the news with the given id
     * @param id of news
     * @return NewsVO(news,author,tags,comments) object with given id
     * @throws ServiceException
     */
    NewsVO findNewsVO(Long id) throws ServiceException;

    /**
     * Build NewsVO list by News list
     * @param newsList
     * @return NewsVO list
     * @throws ServiceException
     */
    List<NewsVO> buildNewsVOList(List<News> newsList) throws ServiceException;

    /**
     * Edits newsVO(news, news author, news tags)
     * @param news
     * @param authorId
     * @param tags
     * @throws ServiceException
     */
    void editNewsVO(News news, Long authorId, List<Tag> tags) throws ServiceException;

    public void setNewsService(INewsService newsService);

    public void setTagService(ITagService tagService);

    public void setCommentService(ICommentService commentService);

    public void setAuthorService(IAuthorService authorService);

}
