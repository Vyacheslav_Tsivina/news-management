package com.epam.news.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
/**
 * Class to store comment info
 *
 */
public class Comment implements Serializable{
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -5697896094322498108L;
    /**
     * Comment id
     */
    private long id;
    /**
     * Comment text
     */
    private String commentText;
    /**
     * Comment creation date
     */
    private Date creationDate;
    /**
     * Id of the news which the comment is added to
     */
    private long newsId;

    public Comment() {}

    public Comment(long id, String commentText, Date creationDate,
                   long news_id) {
        super();
        this.id = id;
        this.commentText = commentText;
        this.creationDate = creationDate;
        this.newsId = news_id;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getCommentText() {
        return commentText;
    }
    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }
    public Date getCreationDate() {
        return creationDate;
    }
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
    public long getNewsId() {
        return newsId;
    }
    public void setNewsId(long newsId) {
        this.newsId = newsId;
    }
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("Comment [id=");
        str.append(id);
        str.append(", commentText=");
        str.append(commentText);
        str.append(", creationDate=");
        str.append(creationDate);
        str.append(", newsId=");
        str.append(newsId);
        str.append("]");
        return str.toString();
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((commentText == null) ? 0 : commentText.hashCode());
        result = prime * result
                + ((creationDate == null) ? 0 : creationDate.hashCode());
        result = prime * result + (int) (id ^ (id >>> 32));
        result = prime * result + (int) (newsId ^ (newsId >>> 32));
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Comment other = (Comment) obj;
        if (commentText == null) {
            if (other.commentText != null)
                return false;
        } else if (!commentText.equals(other.commentText))
            return false;
        if (creationDate == null) {
            if (other.creationDate != null)
                return false;
        } else if (!creationDate.equals(other.creationDate))
            return false;
        if (id != other.id)
            return false;
        if (newsId != other.newsId)
            return false;
        return true;
    }


}
