package com.epam.news.entity;

import java.io.Serializable;
import java.util.List;

public class FilterVO implements Serializable {
    private List<Tag> tagsSearch;
    private Author authorSearch;

    public FilterVO() {
    }

    public FilterVO(List<Tag> tagsSearch, Author authorSearch) {
        this.tagsSearch = tagsSearch;
        this.authorSearch = authorSearch;
    }

    public List<Tag> getTagsSearch() {
        return tagsSearch;
    }

    public void setTagsSearch(List<Tag> tagsSearch) {
        this.tagsSearch = tagsSearch;
    }

    public Author getAuthorSearch() {
        return authorSearch;
    }

    public void setAuthorSearch(Author authorSearch) {
        this.authorSearch = authorSearch;
    }
}
