package com.epam.news.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Locale;

/**
 * Class to store news info
 *
 */
public class News implements Serializable{
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 883279937885116359L;
    /**
     * News id
     */
    private long id;
    /**
     * News short text
     */
    private String shortText;
    /**
     * News full text
     */
    private String fullText;
    /**
     * News title
     */
    private String title;
    /**
     * News creation date
     */
    private Date creationDate;
    /**
     * News modification date
     */
    private Date modificationDate;

    public News() {}

    public News(long id, String shortText, String fullText, String title,
                Date creationDate, Date modificationDate) {

        this.id = id;
        this.shortText = shortText;
        this.fullText = fullText;
        this.title = title;
        this.creationDate = creationDate;
        this.modificationDate = modificationDate;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getShortText() {
        return shortText;
    }
    public void setShortText(String shortText) {
        this.shortText = shortText;
    }
    public String getFullText() {
        return fullText;
    }
    public void setFullText(String fullText) {
        this.fullText = fullText;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public Date getCreationDate() {
        return creationDate;
    }
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
    public Date getModificationDate() {
        return modificationDate;
    }
    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("NewsEntity [id=");
        str.append(id);
        str.append(", shortText=");
        str.append(shortText);
        str.append(", fullText=");
        str.append(fullText);
        str.append(", title=");
        str.append(title);
        str.append(", creationDate=");
        str.append(creationDate);
        str.append(", modificationDate=");
        str.append(modificationDate);
        str.append("]");
        return str.toString();
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((creationDate == null) ? 0 : creationDate.hashCode());
        result = prime * result
                + ((fullText == null) ? 0 : fullText.hashCode());
        result = prime * result + (int) (id ^ (id >>> 32));
        result = prime
                * result
                + ((modificationDate == null) ? 0 : modificationDate.hashCode());
        result = prime * result
                + ((shortText == null) ? 0 : shortText.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        News other = (News) obj;
        if (creationDate == null) {
            if (other.creationDate != null)
                return false;
        } else if (!creationDate.equals(other.creationDate))
            return false;
        if (fullText == null) {
            if (other.fullText != null)
                return false;
        } else if (!fullText.equals(other.fullText))
            return false;
        if (id != other.id)
            return false;
        if (modificationDate == null) {
            if (other.modificationDate != null)
                return false;
        } else if (!modificationDate.equals(other.modificationDate))
            return false;
        if (shortText == null) {
            if (other.shortText != null)
                return false;
        } else if (!shortText.equals(other.shortText))
            return false;
        if (title == null) {
            if (other.title != null)
                return false;
        } else if (!title.equals(other.title))
            return false;
        return true;
    }


}
