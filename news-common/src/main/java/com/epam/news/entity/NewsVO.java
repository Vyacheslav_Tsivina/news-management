package com.epam.news.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/**
 * Class to store full information about news
 *
 */
public class NewsVO  implements Serializable{
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 6018436941915495917L;
    /**
     * News information
     */
    private News news;
    /**
     * List of news tags
     */
    private List<Tag> tags;
    /**
     * List of news comments
     */
    private List<Comment> comments;
    /**
     * News author
     */
    private Author author;

    public NewsVO() {}

    public NewsVO(News news, List<Tag> tags,
                  List<Comment> comments, Author author) {
        this.news = news;
        this.tags = tags;
        this.comments = comments;
        this.author = author;
    }
    public News getNews() {
        return news;
    }
    public void setNews(News news) {
        this.news = news;
    }
    public List<Tag> getTags() {
        return tags;
    }
    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }
    public List<Comment> getComments() {
        return comments;
    }
    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
    public Author getAuthor() {
        return author;
    }
    public void setAuthor(Author author) {
        this.author = author;
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((author == null) ? 0 : author.hashCode());
        result = prime * result
                + ((comments == null) ? 0 : comments.hashCode());
        result = prime * result + ((news == null) ? 0 : news.hashCode());
        result = prime * result + ((tags == null) ? 0 : tags.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        NewsVO other = (NewsVO) obj;
        if (author == null) {
            if (other.author != null)
                return false;
        } else if (!author.equals(other.author))
            return false;
        if (comments == null) {
            if (other.comments != null)
                return false;
        } else if (!comments.equals(other.comments))
            return false;
        if (news == null) {
            if (other.news != null)
                return false;
        } else if (!news.equals(other.news))
            return false;
        if (tags == null) {
            if (other.tags != null)
                return false;
        } else if (!tags.equals(other.tags))
            return false;
        return true;
    }
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("NewsValue [news=");
        str.append(news);
        str.append(", tags=");
        str.append(tags);
        str.append(", comments=");
        str.append(comments);
        str.append(", author=");
        str.append(author);
        str.append("]");
        return str.toString();
    }

}
