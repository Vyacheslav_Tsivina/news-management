package com.epam.news.dao.impl;

import com.epam.news.dao.ICommentDAO;
import com.epam.news.entity.Comment;
import com.epam.news.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Test C.R.U.D operations from CommentDAO
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-configuration-test.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@Transactional
public class CommentDAOTest {
    private final static String FULL_DB_PATH="classpath:fullDB.xml";
    @Autowired
    ICommentDAO commentDAO;

    public CommentDAOTest() {
        super();
    }

    /**
     * Test add method
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testAdd() throws DAOException {

        Comment comment = new Comment(1, "qwerty", null, 1);
        long id = commentDAO.add(comment);
        comment.setId(id);
        Assert.assertEquals(comment, commentDAO.findById(id));

    }

    /**
     * Test edit
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testEdit() throws DAOException {
        Comment comment = new Comment(1, "qwerty", null, 1);
        boolean updateFlag = commentDAO.update(comment);
        Assert.assertTrue(updateFlag);
    }

    /**
     * Test delete
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testDelete() throws DAOException {
        Comment comment = new Comment(1, "qwerty", null, 1);
        long id = commentDAO.add(comment);
        boolean deleteFlag = commentDAO.delete(id);
        Assert.assertTrue(deleteFlag);
    }

    /**
     * Test find method
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testFind() throws DAOException {
        Comment comment = commentDAO.findById(1L);
        Assert.assertNotNull(comment);
    }

    /**
     * Test delete comments for news
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testDeleteCommentsForNews() throws DAOException {
        commentDAO.deleteCommentsForNews(9L);
        List<Comment> comments = commentDAO.findAll();
        Assert.assertEquals(16, comments.size());
    }
}
