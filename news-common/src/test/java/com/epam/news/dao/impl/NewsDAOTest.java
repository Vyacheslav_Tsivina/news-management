package com.epam.news.dao.impl;


import com.epam.news.entity.Author;
import com.epam.news.entity.FilterVO;
import com.epam.news.entity.News;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
/**
 * Test C.R.U.D and other operations from NewsDAO
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-configuration-test.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@Transactional
public class NewsDAOTest{
    private final static String FULL_DB_PATH="classpath:fullDB.xml";

    @Autowired
    private NewsDAOImpl newsDAO;
    private FilterVO filterVO = new FilterVO(null,null);

    public NewsDAOTest(){
        super();
        }

    /**
     * Test add method
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testAdd() throws DAOException
    {
            News news = new News(15,"q","q","q",new Timestamp(1427087631840L),new Timestamp(1427087631840L));
            long id = newsDAO.add(news);
            news.setId(id);

            News actualNews = newsDAO.findById(news.getId());

            assertEquals(news, actualNews);
    }

    /**
     * Test findById method
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testFindById() throws DAOException
    {
            News news = newsDAO.findById(1L);
            assertNotNull(news);
    }
    /**
     * Test if delete successful
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testDeleteNewsSuccess() throws DAOException
    {
            News news = new News(15,"q","q","q",new Timestamp(1427087631840L),new Timestamp(1427087631840L));
            long id = newsDAO.add(news);
            boolean deleteFlag = newsDAO.delete(id);
            assertTrue(deleteFlag);//if delete successful
    }
    /**
     * Test update method
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testUpdate() throws DAOException
    {
            News news = new News(1,"q","q","q",new Timestamp(1427087631840L),new Timestamp(1427087631840L));
            boolean updateFlag = newsDAO.update(news);
            assertTrue(updateFlag);//if update successful

    }
    /**
     * Test find news by author
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testFindByAuthor() throws DAOException
    {
            Author author = new Author(1L,"",null);
            List<News> news = newsDAO.findByAuthor(author);
            assertEquals(3, news.size());
    }

    /**
     * Test newsNumberFromFilter
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testNewsNumberFromFilter() throws DAOException
    {
        Integer expectedNumber = 1;
        assertEquals(expectedNumber, newsDAO.newsNumberFromFilter(9L, filterVO));
    }
    /**
     * Test findNewsByFilters by author
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testFindNewsByAuthor() throws DAOException
    {
        filterVO.setAuthorSearch(new Author(1L,"",null));
        List<News> newsFilter = newsDAO.findNewsByFilters(10,1,filterVO);
        assertEquals(3, newsFilter.size());
    }
    /**
     * Test findNewsByFilters by tag/tags
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testFindNewsByTags() throws DAOException
    {
        List<Tag> tags = new ArrayList<Tag>();
        tags.add(new Tag(21L,"NATO"));
        filterVO.setTagsSearch(tags);
        List<News> newsFilter = newsDAO.findNewsByFilters(10,1,filterVO);
        assertEquals(3, newsFilter.size());

        tags.add(new Tag(22L, "Europe"));
        filterVO.setTagsSearch(tags);
        newsFilter = newsDAO.findNewsByFilters(10,1,filterVO);
        assertEquals(3, newsFilter.size());
    }

    /**
     * Test findNewsByFilters by tags and author
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testFindNewsByAuthorTags() throws DAOException
    {
        Author author = new Author (1L,"",null);
        List<Tag> tags = new ArrayList<Tag>();
        tags.add(new Tag(15L,"military"));

        filterVO.setTagsSearch(tags);
        filterVO.setAuthorSearch(author);

        List<News> newsFilter = newsDAO.findNewsByFilters(10,1,filterVO);
        assertEquals(2, newsFilter.size());

        tags.add(new Tag(14L,"protest"));
        newsFilter = newsDAO.findNewsByFilters(10,1,filterVO);
        assertEquals(2, newsFilter.size());
    }

    /**
     * Test findNewsByFilters without searchCriteria
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testFindNews() throws DAOException
    {
        List<News> newsFilter = newsDAO.findNewsByFilters(10,1,filterVO);
        assertEquals(9, newsFilter.size());
    }
    @Before
    public void resetFilter()
    {
        filterVO.setTagsSearch(null);
        filterVO.setAuthorSearch(null);
    }
}