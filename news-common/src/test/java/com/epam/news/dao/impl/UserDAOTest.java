package com.epam.news.dao.impl;

import com.epam.news.dao.IUserDAO;
import com.epam.news.entity.User;
import com.epam.news.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * Test C.R.U.D operations from TagDAO
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-configuration-test.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@Transactional
public class UserDAOTest{
    private final static String FULL_DB_PATH="classpath:fullDB.xml";
    @Autowired
    private IUserDAO userDAO;


    public UserDAOTest(){
        super();
    }

    /**
     * Test find all
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testFindAll() throws DAOException
    {
        List<User> userList = userDAO.findAll();
        assertEquals(5,userList.size());
    }

    /**
     * Test find by id
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testFindById() throws DAOException
    {
        User expectedUser = new User(1L,"admin","admin","d033e22ae348aeb5660fc2140aec35850c4da997",null);
        User actualUser = userDAO.findById(1L);
        assertEquals(expectedUser,actualUser);
    }
    /**
     * Test add user
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testAddUser() throws DAOException
    {
        User user = new User(0L,"a","a","a",null);

        user.setId(userDAO.add(user));

        User actualUser = userDAO.findById(user.getId());

        assertEquals(user,actualUser);
    }
    /**
     * Test delete user
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testDeleteUser() throws DAOException
    {
        User user = new User(0L,"","","",null);
        Long id = userDAO.add(user);

        boolean deleteFlag = userDAO.delete(id);
        assertTrue(deleteFlag);
    }
    /**
     * Test find by login
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testFindByLogin() throws DAOException
    {
        User expectedUser = new User(1L,"admin","admin","d033e22ae348aeb5660fc2140aec35850c4da997",null);
        User actualUser = userDAO.findByLogin("admin");

        assertEquals(expectedUser,actualUser);
    }

    /**
     * Test update user
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testUpdateUser() throws DAOException
    {
        User user = new User(4L,"admin","admin","d033e22ae348aeb5660fc2140aec35850c4da997",null);

        boolean updateFlag = userDAO.update(user);

        assertTrue(updateFlag);
    }
    /**
     * Test find user role
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testFindUserName() throws DAOException
    {
        String expectedRole = "ROLE_ADMIN";

        String actualRole = userDAO.findUserRole(1L);

        assertEquals(expectedRole,actualRole);
    }
}
