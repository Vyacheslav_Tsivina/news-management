package com.epam.news.dao.impl;

import com.epam.news.dao.ITagDAO;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;
/**
 * Test C.R.U.D operations from TagDAO
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-configuration-test.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@Transactional
public class TagDAOTest{
    private final static String FULL_DB_PATH="classpath:fullDB.xml";

    @Autowired
    ITagDAO tagDAO;

    public TagDAOTest(){
        super();
    }

    /**
     * Test add method
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testAdd() throws DAOException
    {
        Tag tag = new Tag(1,"qwerty");

        long id =tagDAO.add(tag);
        tag.setId(id);
        Assert.assertEquals(tag, tagDAO.findById(id));

    }
    /**
     * Test edit
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testEdit() throws DAOException
    {
        Tag tag = new Tag(1,"qwerty");
        boolean updateFlag =tagDAO.update(tag);
        Assert.assertTrue(updateFlag);
    }
    /**
     * Test  delete
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testDelete() throws DAOException
    {
        Tag tag = new Tag(1,"qwerty");
        boolean deleteFlag =tagDAO.delete(tag.getId());
        Assert.assertTrue(deleteFlag);
    }
    /**
     * Test find method
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testFind() throws DAOException
    {
        Tag tag =tagDAO.findById(1L);
        Assert.assertNotNull(tag);
    }
}
