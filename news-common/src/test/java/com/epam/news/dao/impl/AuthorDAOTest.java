package com.epam.news.dao.impl;



import com.github.springtestdbunit.annotation.DatabaseSetup;

import org.junit.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.dao.IAuthorDAO;
import com.epam.news.entity.Author;
import com.epam.news.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;

/**
 * Test C.R.U.D operations from AuthorDAO
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-configuration-test.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@Transactional
public class AuthorDAOTest {
    private final static String FULL_DB_PATH="classpath:fullDB.xml";

    @Autowired
    private IAuthorDAO authorDAO;
    /**
     * Set properties for DBUnit
     */
    public AuthorDAOTest(){
        super();
    }

    /**
     * Test add method
     * @throws DAOException
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testAdd() throws DAOException
    {

        Author author = new Author(1L,"qwerty",null);
        long id =authorDAO.add(author);
        author.setId(id);
        Assert.assertEquals(author,authorDAO.findById(id));

    }
    /**
     * Test edit
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testEdit() throws DAOException
    {

        Author author = new Author(1L,"qwerty",null);
        boolean updateFlag =authorDAO.update(author);
        Assert.assertTrue(updateFlag);

    }
    /**
     * Test delete
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testDelete() throws DAOException
    {
        Author author = new Author(1L,"qwerty",null);
        long id = authorDAO.add(author);
        boolean deleteFlag =authorDAO.delete(id);
        Assert.assertTrue(deleteFlag);
    }

    /**
     * Test find method
     */
    @Test
    @DatabaseSetup(FULL_DB_PATH)
    public void testFind() throws DAOException
    {
        Author author =authorDAO.findById(1L);
        Assert.assertNotNull(author);
    }
}
