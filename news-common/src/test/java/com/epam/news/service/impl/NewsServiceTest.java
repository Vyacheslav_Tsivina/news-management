package com.epam.news.service.impl;

import java.util.ArrayList;


import com.epam.news.entity.FilterVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


import com.epam.news.dao.impl.NewsDAOImpl;
import com.epam.news.entity.Author;
import com.epam.news.entity.News;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;

import static org.mockito.Mockito.*;
/**
 * Test news service methods
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsServiceTest {
    @Mock
    FilterVO filterVO;
    @Mock
    NewsDAOImpl newsDAO;
    @Mock
    News news;
    @InjectMocks
    NewsServiceImpl newsService;

    /**
     * Test add news
     */
    public void testAddNews()throws DAOException, ServiceException
    {
        newsService.addNews(news);
        when(newsDAO.add(news)).thenReturn(1L);
        verify(newsDAO).add(news);
        verify(news).setId(1L);
    }
    /**
     * Test edit news
     */
    @Test
    public void testEditNews() throws DAOException, ServiceException
    {
        newsService.updateNews(news);
        verify(newsDAO).update(news);
    }
    /**
     * Test delete news
     */
    @Test
    public void testDelteNews() throws DAOException, ServiceException
    {
        newsService.deleteNews(1L);
        verify(newsDAO).delete(1L);
    }
    /**
     * Test news by author
     */
    @Test
    public void testNewsByAuthor() throws DAOException, ServiceException
    {
        Author author = mock(Author.class);
        when(newsDAO.findByAuthor(author)).thenReturn(new ArrayList<News>());
        newsService.newsByAuthor(author);

        verify(newsDAO).findByAuthor(author);
    }


    /**
     * Test view single news
     */
    @Test
    public void testViewSingleNews() throws ServiceException, DAOException
    {
        when(newsDAO.findById(1L)).thenReturn(null);
        newsService.findById(1L);
        verify(newsDAO).findById(1L);
    }

    /**
     * Test findNewsByFilters
     */
    @Test
    public void testFindNewsByFilters() throws ServiceException, DAOException
    {
        newsService.findNewsByFilters(1,10,filterVO);
        verify(newsDAO).findNewsByFilters(1,10,filterVO);
    }

    /**
     * Test find by id
     */
    @Test
    public void testFindById() throws DAOException, ServiceException {
        newsService.findById(1L);
        verify(newsDAO).findById(1L);
    }

    /**
     * Test count news by filter
     */
    @Test
    public void testCountNewsByFilter() throws DAOException, ServiceException {
        newsService.countNewsByFilter(filterVO);
        verify(newsDAO).countNewsByFilter(filterVO);
    }
    /**
     * Test news from filter by number
     */
    @Test
    public void testNewsFromFilterByNumber() throws DAOException, ServiceException {
        newsService.newsFromFilterByNumber(4,filterVO);
        verify(newsDAO).newsFromFilterByNumber(4,filterVO);
    }

    /**
     * Test news number fron filter
     */
    @Test
    public void testNewsNumberFromFilter() throws DAOException, ServiceException {
        newsService.newsNumberFromFilter(1L,filterVO);
        verify(newsDAO).newsNumberFromFilter(1L,filterVO);
    }
}
