package com.epam.news.service.impl;

import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.log4j.PropertyConfigurator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.epam.news.dao.impl.CommentDAOImpl;
import com.epam.news.entity.Comment;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;
/**
 * test comment service methods
 */
@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTest {
    @Mock
    Comment comment;
    @Mock
    CommentDAOImpl commentDAO;

    @InjectMocks
    CommentServiceImpl commentService;


    static {
        ResourceBundle bundle = ResourceBundle.getBundle("path", new Locale("en", "EN"));
        PropertyConfigurator.configure(bundle.getString("log4j.properties"));
    }

    /**
     * Test add comment
     */
    @Test
    public void testAddComment() throws DAOException, ServiceException {

        when(commentDAO.add(comment)).thenReturn(1L);
        commentService.addComment(comment);

        verify(commentDAO).add(comment);
        verify(comment).setId(1L);

    }

    /**
     * Test delete comment
     */
    @Test
    public void testDeleteComment() throws DAOException, ServiceException {

        when(comment.getId()).thenReturn(1L);
        commentService.deleteComment(comment.getId());

        verify(commentDAO).delete(1L);
    }
    /**
     * Test update comment
     */
    @Test
    public void testUpdateComment() throws DAOException, ServiceException {

        commentService.updateComment(comment);

        verify(commentDAO).update(comment);
    }

    /**
     * Test find by id
     */
    @Test
    public void testFindById() throws DAOException, ServiceException {

        commentService.findComment(1L);

        verify(commentDAO).findById(1L);
    }

    /**
     * Test delete comments for news
     */
    @Test
    public void testDeleteCommentsForNews() throws DAOException, ServiceException {

        commentService.deleteCommentsForNews(1L);

        verify(commentDAO).deleteCommentsForNews(1L);
    }

    /**
     * Test find comments for news
     */
    @Test
    public void testFindCommentsForNews() throws DAOException, ServiceException {

        commentService.findCommentsForNews(1L);

        verify(commentDAO).findCommentsForNews(1L);
    }
}
