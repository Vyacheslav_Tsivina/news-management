package com.epam.newsmanagement.controller;

import com.epam.news.entity.*;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.epam.newsmanagement.utils.RequestMappingNames.*;

/**
 * Class provides actions:
 * - add comment
 * - delete comment
 */
@Controller
public class CommentController {
    private final static String REDIRECT_SINGLE_NEWS = "redirect:/single-news/";

    @Autowired
    ICommentService commentService;
    @Autowired
    INewsManageService newsManageService;


    @Secured("ROLE_ADMIN")
    @RequestMapping(value=ADD_COMMENT)
    public ModelAndView addComment(HttpSession session,
                                   @RequestParam(value = "commentText", required = false) String commentText,
                                   @RequestParam(value = "newsId", required = false) Long newsId) throws ServiceException {
        Comment comment = new Comment(0,commentText,new Date(),newsId);
        commentService.addComment(comment);
        NewsVO newsVO = newsManageService.findNewsVO(newsId);
        session.setAttribute("newsVO",newsVO);
        System.out.println(commentText);
        return new ModelAndView(NEWS_SINGLE_VIEW);
    }
    @Secured("ROLE_ADMIN")
    @RequestMapping(value = DELETE_COMMENT)
    public ModelAndView deleteComment(HttpSession session,
                                      @RequestParam(value = "commentId", required = true) Long commentId,
                                      @RequestParam(value = "newsId", required = true) Long newsId) throws ServiceException
    {
        commentService.deleteComment(commentId);
        return new ModelAndView(REDIRECT_SINGLE_NEWS+newsId);
    }

    @ExceptionHandler(ServiceException.class)
    public ModelAndView serviceExceptionHandler()
    {
        return new ModelAndView(ERROR);
    }

}
