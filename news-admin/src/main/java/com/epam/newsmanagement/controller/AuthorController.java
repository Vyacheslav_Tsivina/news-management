package com.epam.newsmanagement.controller;

import com.epam.news.entity.Author;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.IAuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import static com.epam.newsmanagement.utils.RequestMappingNames.*;
/**
 * Class provides actions:
 * - add author
 * - edit author
 * - expire author
 */
@Controller
public class AuthorController {

    @Autowired
    IAuthorService authorService;
    @Secured("ROLE_ADMIN")
    @RequestMapping(UPDATE_AUTHOR)
    public ModelAndView updateAuthor(HttpSession session,
                                     @RequestParam(value="authorId", required = true) Long authorId,
                                     @RequestParam(value = "authorName", required = true) String authorName) throws ServiceException {
        Author author = new Author(authorId,authorName,null);
        authorService.updateAuthor(author);
        List<Author> authorList = authorService.findAllAuthors();
        removeExpiredAuthors(authorList);
        session.setAttribute("authorList",authorList);
        return new ModelAndView(REDIRECT+ ADD_AUTHOR_VIEW);
    }
    @Secured("ROLE_ADMIN")
    @RequestMapping(EXPIRE_AUTHOR)
    public ModelAndView expireAuthor(HttpSession session,
                                     @PathVariable Long authorId) throws ServiceException{
        Author author = authorService.findAuthor(authorId);
        author.setExpired(new Date());
        authorService.updateAuthor(author);
        List<Author> authorList = authorService.findAllAuthors();
        removeExpiredAuthors(authorList);
        session.setAttribute("authorList",authorList);
        return new ModelAndView(REDIRECT+ ADD_AUTHOR_VIEW);
    }
    @Secured("ROLE_ADMIN")
    @RequestMapping(ADD_AUTHOR)
    public ModelAndView addAuthor(HttpSession session,
                                  @RequestParam(value = "authorName", required = true) String authorName)throws ServiceException {
        Author author = new Author(0L,authorName,null);
        authorService.addAuthor(author);
        List<Author> authorList = authorService.findAllAuthors();
        removeExpiredAuthors(authorList);
        session.setAttribute("authorList",authorList);
        return new ModelAndView(REDIRECT+ ADD_AUTHOR_VIEW);
    }

    private void removeExpiredAuthors(List<Author> authorList)
    {
        //remove expired authors
        for (Iterator<Author> iterator = authorList.iterator(); iterator.hasNext();) {
            Author author = iterator.next();
            if (author.getExpired() != null) {
                iterator.remove();
            }
        }
    }
    @ExceptionHandler(ServiceException.class)
    public ModelAndView serviceExceptionHandler()
    {
        return new ModelAndView(ERROR);
    }

}
