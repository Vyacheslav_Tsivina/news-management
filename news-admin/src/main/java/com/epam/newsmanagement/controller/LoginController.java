package com.epam.newsmanagement.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import static com.epam.newsmanagement.utils.RequestMappingNames.*;
/**
 * Class provides login action
 */
@Controller
public class LoginController {

    @RequestMapping(value = LOGIN)
    public ModelAndView loginPage(){
        return new ModelAndView(LOGIN);
    }
}
