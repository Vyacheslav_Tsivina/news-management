package com.epam.newsmanagement.controller;

import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.INewsService;
import com.epam.news.service.ITagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

import static com.epam.newsmanagement.utils.RequestMappingNames.*;
/**
 * Class provides actions:
 * - add tag
 * - edit tag
 * - delete tag
 */
@Controller
public class TagController {

    @Autowired
    ITagService tagService;
    @Autowired
    INewsService newsService;

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = ADD_TAG)
    public ModelAndView addTag(HttpSession session,
                               @RequestParam(value = "tagName", required = true) String tagName) throws ServiceException
    {
        Tag tag = new Tag(0,tagName);
        tagService.addTag(tag);
        return new ModelAndView(REDIRECT+ ADD_TAG_VIEW);
    }
    @Secured("ROLE_ADMIN")
    @RequestMapping(value = UPDATE_TAG)
    public ModelAndView updateTag(HttpSession session,
                                  @RequestParam(value = "tagId", required = true) Long tagId,
                                  @RequestParam(value = "tagName", required = true) String tagName) throws ServiceException
    {
        Tag tag = new Tag(tagId,tagName);
        tagService.updateTag(tag);
        return new ModelAndView(REDIRECT+ ADD_TAG_VIEW);
    }
    @Secured("ROLE_ADMIN")
    @RequestMapping(value = DELETE_TAG)
    public ModelAndView deleteTag(HttpSession session,
                                  @PathVariable Long tagId) throws ServiceException
    {

        List<Long> tagIds = (List<Long>) session.getAttribute("tagIds");
        if (tagIds != null) {
            if (tagIds.contains(tagId)) {//remove tag from search criteria
                tagIds.remove(tagId);
            }
        }
        session.setAttribute("tagIds",tagIds);
        tagService.deleteTagFromNews(tagId);
        tagService.deleteTag(tagId);
        return new ModelAndView(REDIRECT+ ADD_TAG_VIEW);
    }

    @ExceptionHandler(ServiceException.class)
    public ModelAndView serviceExceptionHandler()
    {
        return new ModelAndView(ERROR);
    }
}
