package com.epam.newsmanagement.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class InputDateValue extends TagSupport {
    private Date date;

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int doStartTag() throws JspException {

        SimpleDateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd");

        try {
            pageContext.getOut().write(dateFormat.format(date));
        } catch (IOException e) {
            throw new JspException(e);
        }
        return SKIP_BODY;
    }
}
