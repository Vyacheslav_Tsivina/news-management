package com.epam.newsmanagement.service;

import com.epam.news.entity.User;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.IUserService;
import org.apache.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * Service for spring security
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private static Logger logger= Logger.getLogger(UserDetailsServiceImpl.class);
    private IUserService userService;


    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User user = null;
        UserDetails userDetails = null;
        try {
            user = userService.findUser(login);

            if (user != null){
                Set<GrantedAuthority> roles = new HashSet<>();
                roles.add(new SimpleGrantedAuthority(userService.findUserRole(user.getId())));

                userDetails =
                        new org.springframework.security.core.userdetails.User(user.getLogin(),
                                user.getPassword(),
                                roles);
            }
            else{
                userDetails = new org.springframework.security.core.userdetails.User("non","non", new HashSet<GrantedAuthority>());
            }
        } catch (ServiceException e) {
            logger.error(e);
        }
        return userDetails;
    }

    public void setUserService(IUserService userService) {
        this.userService = userService;
    }
}
