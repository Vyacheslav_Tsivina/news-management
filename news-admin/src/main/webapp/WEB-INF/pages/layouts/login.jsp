<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<div class="login-content"  >
    <form action="/spring_security_check" method="post">
        <h2 class="form-signin-heading"><fmt:message key="login.message.login" /> </h2>
        <input type="text"  name="username"  required autofocus ><br/>
        <input type="password"  name="password"  required ><br />
        <button type="submit"><fmt:message key="login.button.login" /></button>
    </form>
</div>