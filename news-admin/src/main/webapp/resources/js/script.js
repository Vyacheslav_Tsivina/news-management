function switchEnableAuthorInput(inputId) {
        document.getElementById("text"+inputId).disabled = false;
        document.getElementById("edit"+inputId).style.display = "none";
        document.getElementById("update"+inputId).style.visibility = "visible";
        document.getElementById("expire"+inputId).style.visibility = "visible";
        document.getElementById("cancel"+inputId).style.visibility = "visible";
}

function switchEnableTagInput(inputId) {
    document.getElementById("text"+inputId).disabled = false;
    document.getElementById("edit"+inputId).style.display = "none";
    document.getElementById("update"+inputId).style.visibility = "visible";
    document.getElementById("delete"+inputId).style.visibility = "visible";
    document.getElementById("cancel"+inputId).style.visibility = "visible";
}

function updateFormLink(inputId){
    document.forms["updateForm"+inputId].submit();
    return false;
}

function validateNews(){
    var form = document.forms["addNews"];
    var select = form.elements["author"];

    var validationFlag = true;
    var authorSelected=false;
    for (var i = 1; i < select.options.length; i++) {
      var option = select.options[i];
      if(option.selected) {
        authorSelected = true;
      }
    }
    validationFlag = authorSelected;//if we have author
    if (!authorSelected){
        document.getElementById("selectAuthor").style.visibility = "visible";
        validationFlag = false;
    }else{
        document.getElementById("selectAuthor").style.visibility = "hidden";
    }
    if (form.elements['title'].value.length>80){
        document.getElementById("titleValidate").style.visibility = "visible";
        validationFlag = false;
    }else{
        document.getElementById("titleValidate").style.visibility = "hidden";
    }
    if (form.elements['shortText'].value.length>250){
         document.getElementById("shortTextValidate").style.visibility = "visible";
         validationFlag = false;
    }else{
        document.getElementById("shortTextValidate").style.visibility = "hidden";
    }
    if (form.elements['fullText'].value.length>2000){
             document.getElementById("fullTextValidate").style.visibility = "visible";
             validationFlag = false;
    }else{
    document.getElementById("fullTextValidate").style.visibility = "hidden";
    }

    return validationFlag;
}

function deleteNewsValidate(locale){
    var form = document.forms['deleteNews'];

    var newsToDelete = false;
    if (form.elements['newsToDelete'].checked == true){//if there is one one news on page
        return true;
    }
    for (var i=0;i< form.elements['newsToDelete'].length;i++){
        if(form.elements['newsToDelete'][i].checked){
            newsToDelete = true;
            break;
        }
    }
    console.log(newsToDelete);
    if (!newsToDelete){
        if (locale == 'en_EN'){
            alert("Choose some news to delete");
        }else{
            alert("Выберите новость для удаления");
        }
        return false;
    }
    if (locale == 'en_EN'){
        if (confirm('Are you sure you want to delete news?')){
            return true;
        }
    }else{
        if (confirm('Вы уверены, что хотите удалить новость?')){
            return true;
        }
    }

    return false;
}



