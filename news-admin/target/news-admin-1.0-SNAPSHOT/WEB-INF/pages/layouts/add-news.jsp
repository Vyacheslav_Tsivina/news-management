<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="ctg" uri="customtags" %>

<div id="content">
    <form action="/add-news"  name="addNews" method="post" onsubmit="return validateNews()">
        <select  name="author" required>
            <option selected="selected" disabled><fmt:message key="main.message.select.author"/></option>
            <c:forEach items="${authorList}" var="author">
               <option value="<c:out value="${author.id}"/>"><c:out value="${author.name}"/></option>
            </c:forEach>
        </select>
        <select name="tags" id="multi-dropbox" class="multi-dropbox" multiple="multiple">
            <c:forEach items="${tagList}" var="tag">
                <option value="${tag.id}">${tag.name}</option>
            </c:forEach>
        </select><br/>
        <span id="selectAuthor" class="red-color hidden" ><fmt:message key="add.news.message.select.author" /></span></br>
        <table>
            <tr>
                <td><fmt:message key="add.news.message.title" /></td>
                <td>
                    <input type="text" name="title" required size="50" /><br />
                    <span id="titleValidate" class="red-color hidden" ><fmt:message key="add.news.message.title.validate" /></span>
                </td>
            </tr>
            <tr>
                 <td><fmt:message key="add.news.message.short.text" /></td>
                 <td>
                    <input type="text" name="shortText" required size="50" cols="2"/><br />
                    <span id="shortTextValidate" class="red-color hidden" ><fmt:message key="add.news.message.short.text.validate" /></span>
                 </td>
            </tr>
            <tr>
                <td><fmt:message key="add.news.message.full.text" /></td>
                <td>
                    <textarea name="fullText" rows="10" cols="45" required></textarea><br />
                    <span id="fullTextValidate" class="red-color hidden" ><fmt:message key="add.news.message.full.text.validate" /></span>
                </td>
            </tr>
            <tr>
               <td><fmt:message key="add.news.message.creation.date" /></td>
               <td> <input name="creationDate" required type="date" value="<ctg:inputDateValue date="${currentDate}"/>" /></td>
            </tr>
        </table>

        <input type="submit" class="right-float delete-button" value="<fmt:message key="button.add"/> ">
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function() {
                $(".multi-dropbox").dropdownchecklist();
            }
    );
</script>
