<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<div id="content">
    <a href="/news-filter/${pageNumber}"><fmt:message key="link.back"/></a><br/>
    <form action="/edit-news" method="post" onsubmit="return validateAddNews()">
        <select  name="authorId" required>
            <c:if test="${authorEditId != null}">  <option disabled><fmt:message key="main.message.select.author"/></option></c:if>
            <c:if test="${authorEditId == null}">  <option selected="selected" disabled><fmt:message key="main.message.select.author"/></option> </c:if>
            <c:forEach items="${authorList}" var="author">
                <c:if test="${authorEditId != author.id}"><option value="<c:out value="${author.id}"/>"><c:out value="${author.name}"/></option></c:if>
                <c:if test="${authorEditId == author.id}"><option selected="selected" value="<c:out value="${author.id}"/>"><c:out value="${author.name}"/></option></c:if>
            </c:forEach>
        </select>
        <select name="tags" id="multi-dropbox" class="multi-dropbox" multiple="multiple">
            <c:forEach items="${tagList}" var="tag">
                <c:set var="contains" value="false" />
                <c:forEach var="item" items="${tagEditIds}">
                    <c:if test="${item == tag.id}">
                        <c:set var="contains" value="true" />
                    </c:if>
                </c:forEach>
                <c:if test="${contains == true}"> <option selected="selected" value="${tag.id}">${tag.name}</option></c:if>
                <c:if test="${contains != true}"><option value="${tag.id}">${tag.name}</option></c:if>
            </c:forEach>
        </select><br/>
        <span id="selectAuthor" class="red-color hidden" ><fmt:message key="add.news.message.select.author" /></span></br>
        <fmt:message key="add.news.message.title" /><br />
        <input type="text" name="title" value="${news.title}" required size="50" /><br />
        <span id="titleValidate" class="red-color hidden" ><fmt:message key="add.news.message.title.validate" /></span></br>
        <fmt:message key="add.news.message.short.text" /> <br />
        <input type="text" name="shortText" value="${news.shortText}" required size="50" cols="2"/><br />
        <fmt:message key="add.news.message.full.text" /><br />
        <span id="shortTextValidate" class="red-color hidden" ><fmt:message key="add.news.message.short.text.validate" /></span></br>
        <textarea name="fullText" rows="10" cols="50">${news.fullText}</textarea><br />
        <span id="fullTextValidate" class="red-color hidden" ><fmt:message key="add.news.message.full.text.validate" /></span></br>
        <fmt:message key="edit.news.message.modification.date"/><br/>
        <input name="modificationDate" required type="date" value="<ctg:inputDateValue date="${news.modificationDate}"/>"><br/>
        <input type="hidden" name="creationDate" value="${news.creationDate}" />
        <input type="hidden" name="newsId" value="${news.id}"/>
        <input type="submit" value="<fmt:message key="button.save"/> ">
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function() {
                $(".multi-dropbox").dropdownchecklist();
            }
    );
</script>
